var worldWidth = 1600;
var worldHeight = 900;
var script = {fps:25};
var jointDistance=1;
var maxDots = 100;
var stepSize = 1;
var OGseed = {x:1024,y:worldHeight,radius:10,maxDots:100,fillStyle:0};
var leaf = {x:1024,y:worldHeight,radius:10,fillStyle:'#00ff00'}
var branchRates = 0.98;
var branchDirection = 1;
var branchAngle = 1;


//'#'+Math.floor(Math.random()*16777215).toString(16)

function loadVariables(){
    OGseed.radius       = Number(seedRadiusValue.value);
    leaf.radius         = Number(seedRadiusValue.value);
    OGseed.maxDots      = Number(maxJointsValue.value);
    maxDots             = Number(maxJointsValue.value);
    jointDistance       = Number(distJointValue.value);
    script.fps          = Number(growSpeedValue.value);
    leafRates           = Number(leafRateValue.value);
    branchRates         = Number(branchRateValue.value);
    branchDirection     = Number(branchDirectionValue.value);
    branchAngle         = Number(branchAngleValue.value);
    OGseed.x            = Number(seedXValue.value);
    OGseed.y            = Number(seedYValue.value);
}

function drawDot(dot){
  ctx.beginPath();
  ctx.arc(dot.x, dot.y, dot.radius, 0, 2 * Math.PI, false);
  ctx.fillStyle = dot.fillStyle;
  ctx.fill();
}

function onLoad(){
	canvas = document.getElementById("GUICanvas");
	canvas.width = worldWidth;
	canvas.height = worldHeight;
	ctx = canvas.getContext("2d");	
    loadVariables();
	//graphCanvas = document.getElementById("Graph");
	//graphCanvas.width = 500;
	//graphCanvas.height = maxGraph;
	//gctx = graphCanvas.getContext("2d");
    //script.inerval = setInterval(grow,1000/script.fps);
}

function plant(seed){
    //seed.x=Math.random()*worldWidth;
    seed.fillStyle = '#8B4513'; //Math.floor(Math.random()*16777215);
    var tree=[];
    grow(seed,tree);
}

function grow(seed,tree){
    loadVariables()
    tree[0] = seed;
    for (i=0;i<seed.maxDots;i++){
        drawDot(tree[i]);
        tree[i+1]=nextDot(tree[i],tree[0].radius);
        if(Math.random() > branchRates){
            growBranch(tree[i],i);
        }
        //await sleep(10000/script.fps);
    }        
}

function growBranch(tree,i){
    var branch = [];
    branch[0] = tree;
    branch[0].maxDots=tree.maxDots-i;
    direction=(Math.random()>branchDirection);
    for(j=0;j<branch[0].maxDots;j++){
        drawDot(branch[j]);
        if(Math.random()*(j/branch[0].maxDots) > leafRates){
            leaf.x=branch[j].x + branch[j].radius*(Math.random()*2-1);
            leaf.y=branch[j].y;
            drawDot(leaf);
            }
        branch[j+1]=nextBranchDot(branch[j],branch[0].radius,direction);
        //await sleep(5000/script.fps);
    }
}

function nextDot(dot,radius){
    newPos  =(Math.random()*2-1)*dot.radius;
    newX    = dot.x+newPos;
    newY    = dot.y-Math.sqrt(Math.pow((dot.radius)/jointDistance,2)-Math.pow(newPos,2));
    newRad  = dot.radius-(radius/maxDots);
    return {x:newX,y:newY,radius:newRad,fillStyle:dot.fillStyle,maxDots:dot.maxDots};
}

function nextBranchDot(dot,radius,direction){
    newPos  =(Math.random()*2-1)*dot.radius;
    if(direction==true){newX = dot.x-Math.abs(newPos);}
    else{newX = dot.x+Math.abs(newPos);}
    newY    = dot.y-(Math.sqrt(Math.pow((dot.radius)/jointDistance,2)-Math.pow(newPos,2)))*branchAngle;
    newRad  = dot.radius-(radius/dot.maxDots);
    return {x:newX,y:newY,radius:newRad,fillStyle:dot.fillStyle,maxDots:dot.maxDots};    
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function DisplayChange(newvalue,element) { 
    document.getElementById(element).innerHTML = newvalue; 
} 

function clearCanvas(){
    ctx.clearRect(0,0,canvas.width,canvas.height);
}